import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from MyPkg.calc import inc, inc2, dec

#def test_1():
#    assert dec(2) == 1

def test_answer():
    assert inc(3) == 4
    assert inc2(4) == 5
