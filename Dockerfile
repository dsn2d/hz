FROM python:3.9
WORKDIR /code
ADD . /code
RUN python -m pip install -r requirements.txt
CMD ["python", "app.py"]
