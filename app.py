import os
import datetime

from flask import Flask, render_template, url_for, request
from jinja2.environment import Template
import pandas as pd
from dotenv import load_dotenv
import folium


load_dotenv()
app = Flask(__name__)


def docked() -> bool:
    return os.path.exists("/.dockerenv") or os.getenv("CI") == "1"


@app.route("/map")
def folium_map():
    type_ = request.args.get("type", default=1, type=int)
    tokyo = (35.681167, 139.767052)
    tachikawa = (35.697914, 139.413741)
    m = folium.Map(
        location=tokyo,
        zoom_start=14,
    )

    if type_ == 1:
        folium.Marker(tokyo, popup='<i>東京駅</i>').add_to(m)
        folium.Marker(tachikawa, popup='<b>立川駅</b>').add_to(m)
    elif type_ == 2:
        folium.TileLayer('cartodbpositron', name='cartodbpositron').add_to(m)
        g_markers = folium.FeatureGroup(name='markers', show=True).add_to(m)
        g_markers.add_child(
            folium.Marker(
                tokyo,
                popup='<i>東京駅</i>',
                icon=folium.Icon(color='lightgray', icon='home', prefix='fa')
            )
        )
        g_markers.add_child(
            folium.Marker(
                tachikawa,
                popup='<b>立川駅</b>',
                icon=folium.Icon(color='blue', icon='space-shuttle', prefix='fa')
            )
        )
        g_lines = folium.FeatureGroup(name='lines', show=False).add_to(m)
        g_lines.add_child(
            folium.PolyLine(
                (tokyo, tachikawa),
                popup='<i>東京-立川</i>',
                color='red',
                weight=3,
                dash_array='10',
            )
        )
        g_lines.add_child(folium.RegularPolygonMarker(location=tokyo, fill_color='blue', number_of_sides=3, radius=10, rotation=0))
        folium.LayerControl().add_to(m)
        m.fit_bounds([tokyo, tachikawa])
    else:
        pass

    m.save(f'templates/folium/map{type_}.html')
    return render_template(f'folium/map{type_}.html')


@app.route("/")
def root():
    df = pd.DataFrame([{'id': 0, 'name': 'Taro'}, {'id': 1, 'name': 'Roy'}])
#    return f"Hello World! docked:{docked()} {datetime.datetime.utcnow()} {df.describe()}"
    return render_template(
        "index.html",
        title="ROOT",
        font_awesome_js=os.getenv("FONT_AWESOME_JS"),
        docked=docked(),
        dt=datetime.datetime.now())


if __name__ == "__main__":
    print("=================================================")
    print("Docked:", docked())
    print("GOOGLE_API_KEY:", os.getenv("GOOGLE_API_KEY"))
    print("FONT_AWESOME_JS:", os.getenv("FONT_AWESOME_JS"))
    print("================================= Thanks dotenv!!\n")
    app.run(host="0.0.0.0", port=5000, debug=True)
